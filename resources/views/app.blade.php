<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>To-Do</title>

    <link href="{{ asset('css/app.css') }}"/>
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet">
    <link href="{{ mix('css/custom.css') }}" rel="stylesheet">

</head>
<body class="antialiased">
<div class="flex-center position-ref full-height" id="app">
    <app></app>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
