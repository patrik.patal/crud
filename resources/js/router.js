import Vue  from 'vue'
import VueRouter from 'vue-router'

//Components

import Home from './components/pages/Home'

Vue.use(VueRouter);
export default new VueRouter({
    mode: 'history',
    routes: [
        //necessary routes
        {
            path: '/',
            name: 'home',
            component: Home
        },
    ]
})