window.Vue = require("vue").default;
window.axios = require('axios');

import Vue from 'vue'
import router from './router'
import Toasted from 'vue-toasted';

Vue.component('app', require('./components/App').default);
Vue.use(Toasted, {
    duration: 1500,
    position: 'top-right', // ['top-right', 'top-center', 'top-left', 'bottom-right', 'bottom-center', 'bottom-left']
    theme: 'outline', // ['toasted-primary', 'outline', 'bubble']
    iconPack: 'material' // ['material', 'fontawesome', 'mdi', 'custom-class', 'callback']
})

const app = new Vue({
    el: '#app',
    router,
});
