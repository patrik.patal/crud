<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TodoList extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "todo_list";
    protected $guarded = [];

    public function details(){
        return $this->hasOne(TodoList::class, 'id');
    }

}
