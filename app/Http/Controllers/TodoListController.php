<?php

namespace App\Http\Controllers;

use App\Models\TodoList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class TodoListController extends Controller
{
    //
    public function getTodoList(){
        try{
            $todos = TodoList::query()
                ->with('details')
                ->paginate(10);

            return $this->baseResponse(true,"List of todo",$todos);
        }catch (\Exception $e){
            return $this->baseResponse(false,"",null,$e);
        }

    }

    public function removeTodo(Request $request){
        try{
            $todo = TodoList::query()
                ->where('id', $request->id)
                ->delete();

            return $this->baseResponse(true,"Successfully removed todo item",$todo);
        }catch (\Exception $e){
            return $this->baseResponse(false,"",null,$e);
        }
    }

    public function saveTodo(Request $request){
        try{

            $validator = Validator::make($request->all(),[
                'title' => 'required',
                'description' => 'required',
                'schedule' => 'required',
            ]);

            if($validator->fails()){
                $errorMessage = $validator->getMessageBag();
                return $this->baseResponse(false,$errorMessage->first(),null);
            }

            $todo = TodoList::query()
                ->updateOrCreate(
                    ['id' => $request->id],
                    [
                        'title' => $request->title,
                        'description' => $request->description,
                        'schedule' => $request->schedule,
                        'status' => $request->status
                    ]
                );

            return $this->baseResponse(true,"Successfully saved todo item",$todo);
        }catch (\Exception $e){
            return $this->baseResponse(false,"",null,$e);
        }
    }


}
