<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function baseResponse($status, $message, $data, \Exception $exception = null)
    {

        if ($status == true) {
            return response()->json(array(
                "status_code" => 200,
                "success" => $status,
                "message" => $message,
                "result" => $data
            ));
        } else {
            return response()->json(array(
                "status_code" => $exception != null ? $exception->getCode() : 500,
                "success" => $status,
                "message" => $exception != null ? $exception->getMessage() : $message,
                "file" => $exception != null ? $exception->getFile() : null,
                "line" => $exception != null ? $exception->getLine() : null,
                "result" => $data
            ));
        }
    }
}
